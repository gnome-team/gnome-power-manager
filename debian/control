Source: gnome-power-manager
Section: gnome
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Jeremy Bícha <jbicha@ubuntu.com>,
           Laurent Bigonville <bigon@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-gnome,
               appstream-util,
               docbook-to-man,
               docbook-utils,
               libcairo2-dev (>= 1.0.0),
               libglib2.0-dev (>= 2.45.8),
               libgtk-3-dev (>= 3.4.0),
               libupower-glib-dev (>= 0.99),
               meson (>= 0.46.0),
               pkgconf
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/gnome-team/gnome-power-manager
Vcs-Git: https://salsa.debian.org/gnome-team/gnome-power-manager.git
Homepage: https://projects.gnome.org/gnome-power-manager/

Package: gnome-power-manager
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: default-dbus-session-bus | dbus-session-bus,
         gnome-settings-daemon-common,
         upower (>= 0.99),
         ${misc:Depends},
         ${shlibs:Depends}
Description: power management tool for the GNOME desktop
 GNOME Power Manager is a session daemon for the GNOME desktop
 that takes care of system or desktop events related to power, and
 triggers actions accordingly. Its philosophy is to completely hide
 these complex tasks and only show some settings important to the user.
 .
 GNOME power manager displays and manages battery status, power plug
 events, display brightness, CPU, graphics card and hard disk drive
 power saving, and can trigger suspend-to-RAM, hibernate or shutdown
 events, all integrated to other components of the GNOME desktop.
